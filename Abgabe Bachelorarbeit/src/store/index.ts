import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import executionState from '../store/stateOfExecution/state-execution'
import programCodeState from '../store/programCode/program-code'
import stackShemaState from '../store/stackSchema/stack-shema'

Vue.use(Vuex)

//Hier wird der Store in drei Abschnitte Modularisiert
const store: StoreOptions<any> = {
  modules: {
    programCodeState,
    stackShemaState,
    executionState
  },
}
export default new Vuex.Store(store);

