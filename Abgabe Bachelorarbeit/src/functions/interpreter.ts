import { Method } from '../store/programCode/types'
import { StackUnit, Actions } from '../store/stackSchema/types'
import store from '../store/index'
import { removeSemicolon } from './interpreter-utils'

var stackShema: Array<StackUnit>;
const variableDefinitionRegEx = new RegExp(/^(\s*)int(\s*)[a-z][a-zA-Z_0-9]*(\s*);/);
const variableAllocationRegEx = new RegExp(/^\s*([a-z])(([a-zA-Z_0-9]*)+)*(\s*)=(\s*)-?(\s*)(([0-9])|([a-z]))((\w*)+)*(\s*);/);
const variableArithmethicRegEx = new RegExp(/^(\s*)([a-z])((\w)+)*(\s*)=(\s*)(([0-9])|([a-z]))((\w)+)*(\s*)(\+|-|\*|\/)(\s*)(([0-9])|([a-z]))((\w)+)*(\s*);/);
const methodCallRegEx = new RegExp(/^\s*(([a-z])((\w)+)* = )?([a-z])((\w)+)*\(\s*((([0-9])|([a-z]))((\w)+)*(,(([0-9])|([a-z]))((\w)+)*)*)*\);/);
const ifRegEx = RegExp(/^\s*if \((([0-9])|([a-z]))((\w)+)* (\>|\>=|==|\<|\<=) (([0-9])|([a-z]))((\w)+)*\)(\s)*{/);
const elseRegEx = RegExp(/^\s*}(\s)*else(\s)*{/);
const returnRegEx = RegExp(/^(\s*)return(\s*)([a-z])((\w)+)*(\s*);/);

function programInterpreter(method: Method) {
    stackShema = [];
    try {
        mainMethodInterpreter(method);
    } catch (e) {
        return e;
    }
    return stackShema;
}

function mainMethodInterpreter(method: Method) {
    var uuid = Math.floor(Math.random() * 100)
    var color = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
    codeInterpreterRegEx(method, uuid, color)
    //Ermittlung der zu löschenden Einträge, welche in der Main-Methode definiert wurden
    let toDeletableUnits = stackShema.filter(deletableUnits => deletableUnits.methodName === "main" + "," + uuid && (deletableUnits.action === Actions.insert))
    toDeletableUnits.reverse().forEach(deletUnit => stackShema.push({ name: deletUnit.name, value: deletUnit.value, methodName: "main" + "," + uuid, action: Actions.bulkDelete, programRow: -1, color: color }))
}

function calledMethodInterpreter(method: Method, parameterValue: Array<number>, originProgramRow: number, calledMethod: string) {
    var uuid = Math.floor(Math.random() * 100)
    var color = "#" + ((Math.random() * 0xffffff) << 0).toString(16)
    parameterAndHeaderInterpreter(method, parameterValue, uuid, originProgramRow, calledMethod, color)
    return codeInterpreterRegEx(method, uuid, color)
}

function parameterAndHeaderInterpreter(method: Method, parameterValue: Array<number>, uuid: number, originProgramRow: number, calledMethod: string, color: string) {
    var methodNameUuid = method.methodHeader.methodName + "," + uuid;
    stackShema.push({
        name: 'Ruecksprungadr.',
        value: '*',
        methodName: methodNameUuid,
        action: Actions.bulkInsert,
        programRow: originProgramRow,
        originMethodName:
            calledMethod,
        color: color
    });
    if (method.methodHeader.returnStatement != "void")
        stackShema.push({
            name: 'Rueckgabewert',
            value: undefined,
            methodName: methodNameUuid,
            action: Actions.bulkInsert,
            programRow: originProgramRow,
            originMethodName: calledMethod,
            color: color
        });
    //Überprüfung, ob die Menge der aktuellen Parameter, mit den der formalen Parameter übereinstimmt
    if (parameterValue.length != method.methodHeader.parameter.length)
        throw new Error(`\nMethode: ${method.methodHeader.methodName}\nParameter Anzahl stimmt nicht`)
    //Erstellung eines Eintrags, für jeden formalen Parameter
    for (let i = 0; i < method.methodHeader.parameter.length; i++) {
        let stackUnit: StackUnit =
        {
            name: method.methodHeader.parameter[i].split(' ')[1],
            value: parameterValue[i],
            methodName: methodNameUuid,
            action: Actions.bulkInsert,
            isParameter: true,
            programRow: originProgramRow,
            originMethodName: calledMethod,
            color: color
        };
        stackShema.push(stackUnit)
    }
}
function codeInterpreterRegEx(method: Method, uuid: number, color: string) {
    //lokale Variablen
    var methodNameUuid = method.methodHeader.methodName + "," + uuid;
    var tempAttributes: Array<string> = [];
    var suitableParameter = stackShema.filter(unit => unit.methodName === methodNameUuid && unit.isParameter == true)
    var codeLines: Array<string> = method.methodBody.split('\n');
    var ifElseStatement: boolean = true;

    //Durchlaufen jeder Programmzeile im Methodenkörper
    for (let i = 0; i < codeLines.length; i++) {
        //Entfernung der möglichen vorranstehenden Einrückungen
        while (codeLines[i].search('\t') == 0) {
            let temp = codeLines[i].split('\t');
            codeLines[i] = temp[temp.length - 1];
        }

        // Interpretation einer Variablen Definition    2.2.3.1 
        if (variableDefinitionRegEx.test(codeLines[i])) {
            let variableName = codeLines[i].split(/\s+/)[1]
            variableName = removeSemicolon(variableName)
            //Überprüfung, ob Variable bereit in der Methode existiert
            if (tempAttributes.includes(variableName) || suitableParameter.find(parameter => parameter.name === variableName))
                throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nlokale Variable "${variableName}" wurde in dieser Methode bereits deklariert`)
            tempAttributes.push(variableName)
            stackShema.push({
                name: variableName,
                value: undefined,
                methodName: methodNameUuid,
                action: Actions.insert,
                programRow: i,
                color: color
            })
        }

        // Interpretation einer Variablen Zuweisung    2.2.3.2
        else if (variableAllocationRegEx.test(codeLines[i])) {
            let firstVariable = codeLines[i].split(/\s+/)[0]
            let negate: boolean = false;
            //Überprüfung, ob der erste Term bereits in der Methode als Variable existiert
            if (tempAttributes.includes(firstVariable)) {
                var secondVariable = removeSemicolon(codeLines[i].split(/\s+/)[2])
                //Überprüfung, ob eine Negierung erfolgt
                if (secondVariable.startsWith('-')) {
                    secondVariable = secondVariable.split('-')[1];
                    negate = true;
                }
                let zuweisungsWert: number = 0;
                //Überprüfung, ob der zweite Term bereits in der Methode existiert
                if (tempAttributes.includes(secondVariable) || suitableParameter.find(parameter => parameter.name === secondVariable)) {
                    zuweisungsWert = Number(stackShema.reverse().find(stackUnit => stackUnit.value != undefined && stackUnit.name === secondVariable)?.value)
                    stackShema.reverse();
                    // Falss nicht, ob es sich um eine Ganzzahl handelt
                } else if (!isNaN(Number(secondVariable)))
                    zuweisungsWert = Number(secondVariable);
                else {
                    throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nVariable "${secondVariable}" nicht deklariert`)
                }
                if (negate)
                    zuweisungsWert = zuweisungsWert * -1
                stackShema.push({
                    name: firstVariable,
                    value: zuweisungsWert,
                    methodName: methodNameUuid,
                    action: Actions.update,
                    programRow: i,
                    color: color
                })
            } else
                throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nVariable "${firstVariable}" nicht deklariert`)
        }

        // Interpretation einer Arithmetischen Variablen Zuweisung    2.2.3.3 
        else if (variableArithmethicRegEx.test(codeLines[i])) {
            //lokale Variablen
            var codeLine = codeLines[i].split(/\s+/)
            let firstVariable = codeLine[0];
            let secondVariable = codeLine[2]
            let thirdVariable = removeSemicolon(codeLine[4])
            let ersterZuweisungsWert: number | undefined = undefined, zweiterZuweisungswert: number | undefined = undefined;
            let ergebnis: number = 0;
            //Überprüfung, ob zuweisungsvariable bereits in der Methode als Variable existiert
            if (tempAttributes.includes(firstVariable) == false) {
                throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nVariable "${firstVariable}" nicht deklariert`)
            }
            //Überprüfung, ob der linke Tern bereits in der Methode existiert
            if (tempAttributes.includes(secondVariable) || suitableParameter.find(parameter => parameter.name === secondVariable)) {
                ersterZuweisungsWert = Number(stackShema.reverse().find(stackUnit => stackUnit.value != undefined && stackUnit.name === secondVariable && stackUnit.methodName === methodNameUuid)?.value)
                stackShema.reverse();
                // Falss nicht, ob es sich um eine Ganzzahl handelt
            } else if (!isNaN(Number(secondVariable)))
                ersterZuweisungsWert = Number(secondVariable);
            else
                throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nVariable "${secondVariable}" nicht deklariert`)
            //Überprüfung, ob der linke Tern bereits in der Methode existiert
            if (tempAttributes.includes(thirdVariable) || suitableParameter.find(parameter => parameter.name === thirdVariable)) {
                zweiterZuweisungswert = Number(stackShema.reverse().find(stackUnit => stackUnit.value != undefined && stackUnit.name === thirdVariable && stackUnit.methodName === methodNameUuid)?.value)
                stackShema.reverse();
                // Falss nicht, ob es sich um eine Ganzzahl handelt
            } else if (!isNaN(Number(thirdVariable)))
                zweiterZuweisungswert = Number(thirdVariable);
            else
                throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nVariable "${thirdVariable}" nicht deklariert`)
            //Verrechnung der beiden Terme
            switch (codeLine[3]) {
                case "+":
                    ergebnis = +ersterZuweisungsWert + +zweiterZuweisungswert;
                    break;
                case "-":
                    ergebnis = +ersterZuweisungsWert - +zweiterZuweisungswert;
                    break;
                case "*":
                    ergebnis = +ersterZuweisungsWert * +zweiterZuweisungswert;
                    break;
                case "/":
                    ergebnis = +ersterZuweisungsWert / +zweiterZuweisungswert;
                    break;
            }
            stackShema.push({ name: codeLine[0], value: ergebnis, methodName: methodNameUuid, action: Actions.update, programRow: i, color: color })
        }

        //Interpretation einer IF-Abfrage   2.2.3.4
        else if (ifRegEx.test(codeLines[i])) {
            stackShema.push({
                name: 'IF-Abfrage',
                value: undefined,
                methodName: methodNameUuid,
                action: Actions.skip,
                programRow: i,
                color: color
            })
            codeLine = codeLines[i].split(' ');
            var firstComporator = codeLine[1].split('(')[1]
            var secondComporator = codeLine[3].split(')')[0]
            let firstComporatorValue: number, secondComporatorValue: number;
            //Überprüfung, ob die erste Vergleichsvariable bereits in der Methode existiert
            if (tempAttributes.includes(firstComporator) || suitableParameter.find(parameter => parameter.name === firstComporator)) {
                firstComporatorValue = Number(stackShema.reverse().find(stackUnit => stackUnit.name === firstComporator)?.value)
                stackShema.reverse();
                // Falss nicht, ob es sich um eine Ganzzahl handelt
            } else if (!isNaN(Number(firstComporator)))
                firstComporatorValue = Number(firstComporator)
            else
                throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nVergleichoperator "${firstComporator}" nicht deklariert`)
            //Überprüfung, ob die zweite Vergleichsvariable bereits in der Methode existiert
            if (tempAttributes.includes(secondComporator) || suitableParameter.find(parameter => parameter.name === secondComporator)) {
                secondComporatorValue = Number(stackShema.reverse().find(stackUnit => stackUnit.name === secondComporator)?.value)
                stackShema.reverse();
                // Falss nicht, ob es sich um eine Ganzzahl handelt
            } else if (!isNaN(Number(secondComporator)))
                secondComporatorValue = +secondComporator
            else
                throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nVergleichsoperator "${secondComporator}" nicht deklariert`)
            //Vergleich der beiden Variablen
            if (firstComporatorValue != null && secondComporatorValue != null) {
                switch (codeLine[2]) {
                    case ">=":
                        ifElseStatement = firstComporatorValue >= secondComporatorValue;
                        break;
                    case ">":
                        ifElseStatement = firstComporatorValue > secondComporatorValue;
                        break;
                    case "==":
                        ifElseStatement = firstComporatorValue == secondComporatorValue;
                        break;
                    case "<":
                        ifElseStatement = firstComporatorValue < secondComporatorValue;
                        break;
                    case '<=':
                        ifElseStatement = firstComporatorValue <= secondComporatorValue;
                        break;
                }
                // Bei nicht Erfüllung, Ermittlung des nächsten Else-Zweiges, oder der nächsten geschlossenen geschweiften Klammer
                if (ifElseStatement == false) {
                    for (let k = i; k < codeLines.length; k++) {
                        if (elseRegEx.test(codeLines[k]) || codeLines[k] === "}") {
                            i = k - 1;
                            break;
                        }
                    }
                }
            }
        }

        // Interpretation eines Else-Zweiges    2.2.3.5
        else if (elseRegEx.test(codeLines[i])) {
            //Sofern vorherige IF-Abfrage erfüllt, Ermittlung der nächsten geschlossen geschweiften Klammer
            if (ifElseStatement == true) {
                for (let l = i; l < codeLines.length; l++) {
                    if (codeLines[l] === "}") {
                        i = l;
                        break;
                    }
                }
            }
            else {
                var foundIFStatement: boolean = false;
                //Überprüfung, ob es Zuvor eine IF-Abfrage gab
                for (let m = i; m > 0; m--) {
                    if (ifRegEx.test(codeLines[m])) {
                        foundIFStatement = true;
                        break;
                    } else if (elseRegEx.test(codeLines[m]) || codeLines[m] === "}") {
                        foundIFStatement = false;
                        break;
                    }
                }
                if (!foundIFStatement) {
                    throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nElse Statement hat kein passendes IF-Statement`)
                }
                stackShema.push({
                    name: 'ELSE',
                    value: undefined,
                    methodName: methodNameUuid,
                    action: Actions.skip,
                    programRow: i,
                    color: color
                })
            }

        }

        // Interpretation einer gescheiften geschlossenen Klammer    2.2.3.6
        else if (codeLines[i] === "}") {
            stackShema.push({ name: '}', value: undefined, methodName: methodNameUuid, action: Actions.skip, programRow: i, color: color })
        }

        // Interpretation eines Methodenaufrufes    2.2.3.7
        else if (methodCallRegEx.test(codeLines[i])) {
            let zuwweisungsVariable: string, calledMethod: string, transferParameters: Array<string>, transferParametersValue: Array<number> = [];
            //Überprüfung, ob eine Variablenzuweisung nach dem Methodenaufruf durchgeführt wird
            if (codeLines[i].includes("=")) {
                zuwweisungsVariable = codeLines[i].split(' ')[0];
                calledMethod = codeLines[i].split(' ')[2].split('(')[0]
                transferParameters = codeLines[i].split(' ')[2].split("(")[1].split(');')[0].split(',')

                if (tempAttributes.includes(zuwweisungsVariable) == false) {
                    throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nVariable "${zuwweisungsVariable}" nicht deklariert`)
                }
            }
            else {
                zuwweisungsVariable = ""
                calledMethod = codeLines[i].split('(')[0];
                transferParameters = codeLines[i].split('(')[1].split(');')[0].split(',')
            }
            // Überprüdung, ob der Methodenaufruf aktuelle Parameter besitzt
            if (transferParameters.includes("") == false) {
                transferParametersValue = [];
                //Ermittlung der Werte jedes aktuellen Parameters
                transferParameters?.forEach(transferPara => {
                    //Überprüfung, ob aktueller Parameter bereits in der Methode existiert
                    if (tempAttributes.includes(transferPara) || suitableParameter.find(parameter => parameter.name === transferPara)) {
                        let temp = stackShema.reverse().find(stackUnit => stackUnit.value != undefined && stackUnit.name === transferPara && stackUnit.methodName === methodNameUuid && stackUnit.action === Actions.update)?.value
                        transferParametersValue.push(Number(temp))
                        stackShema.reverse();
                        // Falss nicht, ob es sich um eine Ganzzahl handelt
                    } else if (!isNaN(Number(transferPara)))
                        transferParametersValue.push(Number(transferPara))
                    else
                        throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nParameter "${transferPara}" nicht deklariert`)
                })
            }
            //Holen des Objektes der aufzurufenden Methode aus dem Store
            var calledMethodObject = store.getters['programCodeState/getMethodOfTemporaryProgrammByName'](calledMethod)
            if (calledMethodObject) {
                if (calledMethodObject.methodHeader.returnStatement === "void" && zuwweisungsVariable != "") {
                    throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nDer Rückgabetyp ist "void" Zuweisungsvariable unzulässig`)
                }
                //Aufruf der Funktion, zum Interpretieren der aufzurufenden Methode 
                var returnedObject = calledMethodInterpreter(calledMethodObject, transferParametersValue, i, methodNameUuid)

                if (returnedObject?.returnValue != undefined)
                    stackShema.push({
                        name: zuwweisungsVariable,
                        value: returnedObject.returnValue,
                        methodName: methodNameUuid,
                        action: Actions.update,
                        programRow: i,
                        color: color
                    })
                //Ermittlung der zu löschenden Einträge des Speicherbereichs, der aufgerufenden Methode 
                let toDeletableUnits = stackShema.filter(deletableUnits =>
                    deletableUnits.methodName === calledMethod + "," + returnedObject?.uuidCalledMethod &&
                    (deletableUnits.action === Actions.insert || deletableUnits.action === Actions.bulkInsert)
                )
                toDeletableUnits.reverse().forEach(deletUnit =>
                    stackShema.push({
                        name: deletUnit.name,
                        value: deletUnit.value,
                        methodName: calledMethod + "," + returnedObject?.uuidCalledMethod,
                        action: Actions.bulkDelete,
                        programRow: i,
                        originMethodName: methodNameUuid,
                        color: color
                    }))
            } else
                throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nMethode "${calledMethod}" nicht deklariert`)
        }

        // Interpretation eines Return Statement    2.2.3.8
        else if (returnRegEx.test(codeLines[i])) {

            let returnVariable = codeLines[i].split(/\s+/)[1]
            if (method.methodHeader.returnStatement == "void")
                throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nReturn Statement nicht zulässig, Rückgabetyp der Methode ist ein "void"`)
            returnVariable = removeSemicolon(returnVariable)
            //Überprüfung, ob die Return-Variable in der Methode existiert
            if (tempAttributes.includes(returnVariable) || suitableParameter.find(parameter => parameter.name === returnVariable)) {
                var returnValue = stackShema.reverse().find(stackUnit => stackUnit.name === returnVariable && stackUnit.methodName === methodNameUuid && stackUnit.action === Actions.update)?.value
            }
            else {
                throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nVariable "${returnVariable}" nicht deklariert`)
            }
            if (returnValue) {
                stackShema.reverse();
                stackShema.push({
                    name: 'Rueckgabewert',
                    value: returnValue,
                    methodName: methodNameUuid,
                    action: Actions.update,
                    programRow: i,
                    color: color
                })
            } else
                throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\nRückgabewert "${returnVariable}" nicht deklariert`)
            //Rückgabe eines Objektes mit dem Inhalt des Rückgabewertes und der ID der aktuellen Methode
            return { returnValue: returnValue, uuidCalledMethod: uuid }
        }

        // Auffang einer nicht Unterstützten Programmzeile
        else {
            throw new Error(`\nMethode: ${method.methodHeader.methodName}\nZeile: ${i + 1}\n Programmzeile wird nicht unterstützt`)
        }
    }
    //Nach Beendiguing der Schleife, Überprüfung, ob der Rückgabetyp der Methode vom Typ "void" ist
    if (method.methodHeader.returnStatement == "void") {
        return { returnValue: undefined, uuidCalledMethod: uuid }
    } else {
        throw new Error(`\nMethode: ${method.methodHeader.methodName}\nFehlendes Return Statement`)
    }
}
export { programInterpreter }