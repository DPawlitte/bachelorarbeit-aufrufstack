import Vue from 'vue';
import store from './store';
import vuetify from './plugins/vuetify';
import VueKonva from 'vue-konva';
import App from './App.vue'

Vue.use(VueKonva)
Vue.config.productionTip = false

new Vue({
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
