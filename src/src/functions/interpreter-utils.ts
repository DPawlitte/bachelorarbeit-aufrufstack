function removeSemicolon(variable: string): string {

    if (variable.endsWith(";"))
        return variable.substring(0, variable.length - 1)
    return variable
}
export { removeSemicolon }