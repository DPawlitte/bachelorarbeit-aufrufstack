import { Module, MutationTree, GetterTree } from 'vuex'
import { StackArray, StackUnit } from './types'

import Vue from 'vue'

const defaultState = () => {
    return {
        stack: []
    }
}
export const state: StackArray = defaultState()

//Getters auf den State
export const getters: GetterTree<any, StackArray> = {
    //Holen aller sich im State befindlichen Attribute
    getAllAttributes: (state: StackArray) => () => {
        return state.stack;
    }
}

//Mutations auf den State
export const mutations: MutationTree<StackArray> = {
    //Hinzufügung eines Attribut
    addAttribute: (state: StackArray, attribute: StackUnit) => {
        state.stack.push(attribute)
    },
    //Hinzufügung mehrerer Attribute
    addAttributeBulk: (state: StackArray, attribute: Array<StackUnit>) => {
        attribute.forEach(unit => state.stack.push(unit))
    },
    //Aktualisierung eines Attribut
    updateAttribute: (state: StackArray, attribute: StackUnit) => {
        const attributeC = state.stack.find(stackUnit =>
            stackUnit.name === attribute.name && stackUnit.methodName === attribute.methodName)
        if (attributeC) {
            Vue.set(attributeC, 'value', attribute.value)
        }
        else
            window.alert("NOPE")
    },
    //Entfernung eines Attribut
    removeAttribute: (state: StackArray, attribute: StackUnit) => {
        const attributeDelete = state.stack.find(stackUnit =>
            stackUnit.name === attribute.name && stackUnit.methodName === attribute.methodName)
        if (attributeDelete) {
            state.stack.pop();
        }
    },
    //Hinzufügung mehrerer Attribute
    removeAttributeBulk: (state: StackArray, attribute: Array<StackUnit>) => {
        attribute.forEach(unit => {
            var index = state.stack.find(ff => ff.name === unit.name && ff.methodName === unit.methodName)
            if (index)
                state.stack.splice(attribute.indexOf(index), 1)
        })
    },
    //Zurücksetzung des States
    resetState: (state: StackArray) => {
        Object.assign(state, defaultState())
    }
}
export const stackShemaState: Module<any, StackArray> = {
    namespaced: true,
    state,
    mutations,
    getters
}

export default stackShemaState;