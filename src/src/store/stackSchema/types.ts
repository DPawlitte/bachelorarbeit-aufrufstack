export interface StackArray {
    stack: Array<StackUnit>
}

export interface StackUnit {
    name: string,
    value?: number | string,
    methodName: string,
    action: Actions,
    isParameter?: boolean,
    programRow: number,
    originMethodName?: string,
    color: string
}

export enum Actions {
    insert = "INSERT",
    update = "UPDATE",
    delete = "DELETE",
    bulkInsert = "BULKINSERT",
    bulkDelete = "BULKDELETE",
    skip = "SKIP"
}

