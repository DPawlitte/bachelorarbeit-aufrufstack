export interface ProgramCodeArray {
    codeSafer: Array<ProgramCode>
}
export interface ProgramCode {
    programName: string,
    isTemporary: boolean,
    originProgramName?: string
    isProgramPredefined: boolean,
    programCode: Array<Method>;
}

export interface Method {
    methodHeader: {
        returnStatement: string,
        methodName: string,
        parameter: Array<string>
    },
    methodBody: string
}
