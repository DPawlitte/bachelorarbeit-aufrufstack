
import { ProgramCodeArray, Method, ProgramCode } from './types'
import { Module, MutationTree, GetterTree } from 'vuex'

export const state: ProgramCodeArray = {
    codeSafer: []
}
//Mutationen auf den Store
export const mutations: MutationTree<ProgramCodeArray> = {
    //Ein neues Programm wird im State hinterlegt
    addNewProgram(state, programCode: ProgramCode) {
        let program = state.codeSafer.find(code => code.programName === programCode.programName)
        if (program) {
            throw new Error(`\nDas Programm "${programCode.programName}" existiert bereits`)
        } else {
            state.codeSafer.push(programCode)
        }
    },
    //Eine Methode wird dem Temporären Programm hinzugefügt
    addMethodToTemporaryProgram(state, method: Method) {
        let tempCode = state.codeSafer.find(code => code.isTemporary == true)
        if (tempCode?.programCode) {
            tempCode?.programCode.find(methods => {
                if (methods.methodHeader.methodName === method.methodHeader.methodName)
                    throw new Error(`\nDie Methode "${method.methodHeader.methodName}" existiert bereits in diesem Programm`)
            })
            tempCode?.programCode.push(method)

        }
    },
    //Eine Methode aus dem Temporären Programm wird bearbeitet
    editMethodFromTemporaryProgram(state, editMethod: { method: Method, methodNameOld: String }) {
        let tempCode = state.codeSafer.find(code => code.isTemporary == true);
        if (tempCode?.programCode) {
            let toEditableMethod = tempCode.programCode.find(method => method.methodHeader.methodName === editMethod.methodNameOld)
            if (toEditableMethod) {
                if (editMethod.method.methodHeader.methodName !== editMethod.methodNameOld &&
                    tempCode?.programCode.find(method => method.methodHeader.methodName === editMethod.method.methodHeader.methodName)
                ) {
                    throw new Error(`\nDie Methode "${editMethod.method.methodHeader.methodName}" existiert bereits in diesem Programm`)

                }
                let indexOfMethod2 = tempCode.programCode.indexOf(toEditableMethod);
                if (indexOfMethod2 != undefined && indexOfMethod2 > -1) {
                    tempCode.programCode[indexOfMethod2] = editMethod.method;
                }
                else {
                    throw new Error(`\nDie Methode "${editMethod.method.methodHeader.methodName}" existiert nicht`)
                }
            }
        }
    },
    //Eine Methode aus dem Temporären Programm wird entfernt
    removeMethodFromTemporaryProgram(state, method: Method) {
        let tempCode = state.codeSafer.find(code => code.isTemporary == true)
        if (tempCode?.programCode) {
            let indexOfMethod = tempCode?.programCode.indexOf(method);
            if (indexOfMethod && indexOfMethod > -1) {
                tempCode?.programCode.splice(indexOfMethod, 1)
            }
            else {
                throw new Error(`\nDie Methode "${method.methodHeader.methodName}" existiert nicht`)
            }
        }
    },
    //In das Temporäre-Programm wird der Inhalt eines Programme gespeichert
    setTemporary(state, program: ProgramCode) {
        var tempCode = state.codeSafer.find(code => code.isTemporary == true)
        if (tempCode) {
            tempCode.originProgramName = program.programName
            tempCode.programCode = JSON.parse(
                JSON.stringify(program.programCode))
        }
    },
    //Entfernung eins Programmes aus dem State 
    removeProgramByName(state, program: ProgramCode) {
        let deleteProgram = state.codeSafer.find(code => code.programName === program.programName)
        if (deleteProgram) {
            let indexOfDeleteProgram = state.codeSafer.indexOf(deleteProgram);
            if (indexOfDeleteProgram && indexOfDeleteProgram > -1) {
                state.codeSafer.splice(indexOfDeleteProgram, 1)
            }
            else {
                throw new Error(`\nDas Programm "${program.programName}" existiert nicht`)
            }
        }
    }
}

//Getters auf den Store
export const getters: GetterTree<any, ProgramCodeArray> = {
    //Holen des Temporären-Programmes
    getTemporaryProgram: (state: ProgramCodeArray) => (index: boolean): ProgramCode => {
        let tempCode = state.codeSafer.find(code => code.isTemporary == index);
        if (tempCode)
            return tempCode;
        else {
            throw new Error(`\nEs existiert kein Temporäres Programm`)
        }
    },
    //Holen einer Methode aus dem Temporären-Programm
    getMethodOfTemporaryProgrammByName: (state: ProgramCodeArray) => (name: String): Method | undefined => {
        let tempCode = state.codeSafer.find(code => code.isTemporary == true)
        if (tempCode) {
            return tempCode.programCode.find(methods => methods.methodHeader.methodName === name);
        }
        else {
            throw new Error(`\nEs existiert kein Temporäres Programm`)
        }
    },
    //Hollen aller sich im State befindlichen Programme
    getAllPrograms: (state: ProgramCodeArray) => {
        return state.codeSafer.filter(code => code.isTemporary == false);
    },
    //Holen des Programnamens, das sich im Temporären-Programm befindet
    getProgramNameOfTemporaryProgram: (state: ProgramCodeArray) => (index: boolean): string | undefined => {
        let tempCode = state.codeSafer.find(code => code.isTemporary == index);
        if (!tempCode)
            throw new Error(`\nEs existiert kein Temporäres Programm`)
        else
            return tempCode.originProgramName;

    }
}
export const programCodeState: Module<any, ProgramCodeArray> = {
    namespaced: true,
    state,
    mutations,
    getters
}

export default programCodeState;