import { Module, MutationTree, GetterTree } from 'vuex'
import { ExecutionState } from './types'
import { StackUnit } from '../stackSchema/types';


export const state: ExecutionState = {
    methodName: '',
    programRow: 0

}
// Getter auf den State
export const getters: GetterTree<any, ExecutionState> = {
    //Holen der Daten der aktuell ausgeführten Programmzeile
    getExecutionState: (state: ExecutionState) => () => {
        return state;
    }
}
// Mutation auf den State
export const mutations: MutationTree<ExecutionState> = {
    //Setzen der Daten der aktuell ausgeführten Programmzeile
    setExecutionState: (state: ExecutionState, newState: StackUnit) => {
        state.methodName = newState.methodName
        state.programRow = newState.programRow
        state.originMethod = newState.originMethodName
    }
}

export const executionState: Module<any, ExecutionState> = {
    namespaced: true,
    state,
    mutations,
    getters
}

export default executionState;