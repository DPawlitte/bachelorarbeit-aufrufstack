export interface ExecutionState {
    methodName: string,
    programRow: number,
    originMethod?: string
}
